# Project Greenify
Let's keep teams green by tricking it into thinking we are writing stuff

# Requirements
- Microsoft Teams
- Java

# Usage
- Download the latest version from the [releases page](https://gitlab.com/franco-martin/greenify/-/packages/7992241) by clicking the first "x.y.z.zip" file from the list
- Unzip into the desktop
- Open teams
- Open a chat in teams (preferably with a friend or an empty chat)
- In the folder you just unzipped the app, double click the greenify.bat file
- Enter the desired number of minutes to keep teams green or enter 0 if you want it to keep running until you stop it.
- Click on the input box of the chat (where you usually write text)
- Check that words are being written and removed (Meaning greenify is doing its thing)
- Go nap or have some mate
## Troubleshooting
### java is not recognized as an internal or external command
This happens because you dont have java installed.
### The term 'java' is not recognized as te name of a cmdlet...
This happens because you dont have java installed.

# Development
## Compiling the code
```
javac greenify.java
```

## Running the code
```
java greenify
```