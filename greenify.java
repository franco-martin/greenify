import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;    

public class greenify
{
    // measure noqui time
    static long startTime = System.nanoTime();
    static Scanner keyboard = new Scanner(System.in);
    static int minutes = -1;
    public static void main(String[] args) throws IOException, AWTException, InterruptedException
        {
            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    try {
                        Thread.sleep(200);
                        long elapsedTime = System.nanoTime() - startTime;
                        float inMinutes = elapsedTime/1000/1000/1000/60;
                        System.out.println("");
                        System.out.println("You were green for "+inMinutes+" minutes");
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                }
            });


            System.out.println("************************************************************");
            System.out.println("***************** Greenify *********************************");
            System.out.println("************************************************************");
            System.out.println("");
            System.out.println("Enter for how long you want to nap in minutes: (enter 0 for indefinitely)");
            
            while(minutes <0){
                String strMinutes = keyboard.nextLine();
                try {
                    minutes = Integer.parseInt(strMinutes);
                    if(minutes < 0){
                        System.out.println("Try inputing a reasonable number will ya'");
                    }
                }
                catch(NumberFormatException exception){
                    System.out.println("Try inputing a reasonable number will ya'");
                }
            }
            printWarning();
            if(minutes == 0)
                loop(true);
            else
                loop(false);
        }
    static void printWarning() throws InterruptedException {
        for(int i=5; i>0; i--){
            System.out.println("You have "+i+" seconds to leave the cursor in a team's chat...");
            Thread.sleep(1000);
        }
        System.out.println("Go have some mate \u1F9C9 or nap!");
        // Print the time where we started
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
        LocalDateTime now = LocalDateTime.now();  
        System.out.println(dtf.format(now));  
    }
    static void loop(boolean indefinitely) throws AWTException, InterruptedException{   
        if (indefinitely){
            System.out.println("We will keep you green indefinitely, to stop close this window.");
            while(true){
                doWork();
            }
        }
        else{
            while((System.nanoTime() - startTime)/1000/1000/1000/60 < minutes){
                doWork();
                System.out.println("Still have around "+(minutes-((System.nanoTime() - startTime)/1000/1000/1000/60))+" minutes to nap");
            } ;
        }
        
    }
    static void doWork() throws AWTException, InterruptedException{
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_W);
        Thread.sleep(1000);
        robot.keyPress(KeyEvent.VK_O);
        Thread.sleep(1000);
        robot.keyPress(KeyEvent.VK_R);
        Thread.sleep(1000);
        robot.keyPress(KeyEvent.VK_K);
        Thread.sleep(1000);
        robot.keyPress(KeyEvent.VK_BACK_SPACE);
        Thread.sleep(1000);
        robot.keyPress(KeyEvent.VK_BACK_SPACE);
        Thread.sleep(1000);
        robot.keyPress(KeyEvent.VK_BACK_SPACE);
        Thread.sleep(1000);
        robot.keyPress(KeyEvent.VK_BACK_SPACE);
        Thread.sleep(1000);
    }
}